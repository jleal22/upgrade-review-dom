// 1.1 Basandote en el array siguiente, crea una lista ul > li 
// dinámicamente en el html que imprima cada uno de los paises.
const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const $$ul1 = document.createElement('ul');
for (const elem of countries) {
    const $$li1 = document.createElement('li');
    $$li1.textContent = elem;
    $$ul1.appendChild($$li1);
}
document.body.appendChild($$ul1);

// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.
document.querySelector('.fn-remove-me').remove();

// 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
// en el div de html con el atributo data-function="printHere".
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

const $$ul3 = document.createElement('ul');
for (const elem of cars) {
    const $$li3 = document.createElement('li');
    $$li3.textContent = elem;
    $$ul3.appendChild($$li3);
}
document.querySelector('[data-function="printHere"]').appendChild($$ul3);

// 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
// h4 para el titulo y otro elemento img para la imagen.
const countries2 = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

const $$div = document.createElement('div');
$$div.className = "list"

for (const elem of countries2) {
    const $$div4 = document.createElement('div');
    $$div4.className = "list__item"

    const $$h4 = document.createElement('h4');
    $$h4.innerText = elem.title;
    $$h4.className = "list__title"

    const $$img = document.createElement('img');
    $$img.src = elem.imgUrl;
    $$img.alt = elem.title;
    $$img.className="list__img";

    $$div4.appendChild($$h4);
    $$div4.appendChild($$img);
    $$div.appendChild($$div4);
}

document.body.appendChild($$div);

// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
// elemento de la lista.

const clickDeleteLastElement = (event) => {
    console.log('clickDeleteLastElement');
    console.log(event);

    $$item = document.body.querySelector('.list');
    console.log('Lastchild->', $$item.lastChild);
    $$item.removeChild($$item.lastChild);
}

console.log('LastChild2 ->', document.body.lastChild);

const $$button5 = document.createElement('button');
$$button5.innerHTML = "Borrar Ultimo Elemento";
$$button5.addEventListener('click', clickDeleteLastElement);
document.body.appendChild($$button5);

// 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
// elementos de las listas que elimine ese mismo elemento del html.

const clickDeleteButton = (event) => {
    console.log('clickDeleteButton');
    console.log(event.target.parentNode);

    document.body.querySelector('.list').removeChild(event.target.parentNode);
}

const $$list = document.querySelectorAll('.list__item');
for (const elem of $$list) {
    const $$button6 = document.createElement('button');
    $$button6.innerHTML = "Borrar Elemento";
    $$button6.addEventListener('click', clickDeleteButton)
    elem.appendChild($$button6);
}
